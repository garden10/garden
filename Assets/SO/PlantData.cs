﻿using Model;
using UnityEngine;

namespace SO
{
    [UnityEngine.CreateAssetMenu(fileName = "PlantData", menuName = "SO/PlantData", order = 0)]
    public class PlantData : UnityEngine.ScriptableObject
    {
        [SerializeField] private PlantType _type;
        [SerializeField] private float _growTime;
        [SerializeField] private float _exp;
        [SerializeField] private bool _isFade;


        public PlantType Type => _type;
        public float GrowTime => _growTime;
        public float Exp => _exp;
        public bool IsFade => _isFade;
    }
}