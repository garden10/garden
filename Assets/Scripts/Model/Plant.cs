using System;
using UnityEngine;

namespace Model
{
    [Serializable]
    public class Plant
    {
        private const float TransitionStep = 0.5f;
        
        private PlantType _type;
        private float _growTime;
        private float _exp;
        private bool _isFaded;
        
        private GrowStage _currentStage = GrowStage.Seed;
        private float _currentTime;
        public float CurrentTime => _currentTime;
        public GrowStage CurrentStage => _currentStage;
        
        public Action IsHarvest;

        public float GrowTime
        {
            get => _growTime;
            set => _growTime = value;
        }

        public PlantType Type
        {
            get => _type;
            set => _type = value;
        }

        public float Exp
        {
            get => _exp;
            set => _exp = value;
        }

        public bool IsFaded
        {
            get => _isFaded;
            set => _isFaded = value;
        }

        public void Planting() =>
            _currentStage = GrowStage.Sprout;

        public void Update(float _deltaTime)
        {
            if(_currentStage != GrowStage.Seed && _currentStage != GrowStage.Adult && _currentStage != GrowStage.Faded)
            {
                _currentTime += _deltaTime;

                if (_currentTime < _growTime)
                {
                    if (_currentTime / _growTime > TransitionStep)
                        _currentStage = GrowStage.Juvenile;
                }
                else
                    _currentStage = GrowStage.Adult;
            }
        }

        public void Fade() =>
            _currentStage = GrowStage.Faded;

        public void Harvest() =>
            IsHarvest?.Invoke();
    }
}
