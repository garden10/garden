﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Model
{
    [Serializable]
    public class Farmer
    {
        private const float MinDistance = 0.1f;
        
        private Vector3 _startPosition;
        private Vector3 _currentPosition;
        private Vector3 _currentDestination;
        private float _speed;

        private GardenBed _currentGardenBed;
        private ExpManager _expManager;

        private bool _isLifting;
        private bool _isHold;

        private List<GardenBed> _gardenBeds = new List<GardenBed>();
        
        public Action<Vector3> SetPoint;
        public Action IsLifting;
        public Action IsHolding;

        public void Init(ExpManager expManager, Vector3 startPosition)
        {
            _expManager = expManager;
            _startPosition = startPosition;
        }

        public void AddQueue(GardenBed gardenBed)
        {
            if(!_gardenBeds.Contains(gardenBed))
                _gardenBeds.Add(gardenBed);

            if(_gardenBeds.Count == 1)
            {
                _currentGardenBed = gardenBed;
                GoToPoint();
            }
        }
        
        public void GoToPoint()
        {
            _isHold = false;
            _currentDestination = _currentGardenBed?.Position ?? _startPosition;
            
            SetPoint?.Invoke(_currentDestination);
        }

        public void Update(Vector3 position, float speed)
        {
            _currentPosition = position;
            _speed = speed;
            
            if (_currentGardenBed != null && _currentDestination != _startPosition)
            {
                if (Vector3.Distance(_currentPosition, _currentDestination) < MinDistance &&
                    _gardenBeds.Count != 0 && !_isLifting)
                {
                    _isLifting = true;
                    IsLifting?.Invoke();
                }
            }
            else if(Vector3.Distance(_currentPosition, _startPosition) < MinDistance && _speed == 0 && !_isHold)
            {
                _isHold = true;
                IsHolding?.Invoke();
            }
        }
        public void FarmerAction()
        {
            if(_currentGardenBed.CurrentPlant.CurrentStage == GrowStage.Seed)
                _currentGardenBed.Planting();
            else if (_currentGardenBed.CurrentPlant.CurrentStage == GrowStage.Adult)
            {
                _expManager.ExpForPlant(_currentGardenBed.CurrentPlant);
                _currentGardenBed.ReleaseGardenBed();
            }
        }

        public void RemovePlant()
        {
            _gardenBeds.Remove(_currentGardenBed);
            _isLifting = false;
            
            if (_gardenBeds.Count != 0)
                _currentGardenBed = _gardenBeds[0];
            else
                _currentGardenBed = null;
          
            GoToPoint();
        }
    }
}