﻿namespace Model
{
    public enum GrowStage
    {
        Seed,
        Sprout,
        Juvenile,
        Adult,
        Faded
    }
}