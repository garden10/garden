﻿using System;
using UnityEngine;

namespace Model
{
    [Serializable]
    public class FieldManager
    {
        private int _height;
        private int _width;

        public int Height
        {
            get => _height;
            set => _height = value;
        }

        public int Width
        {
            get => _width;
            set => _width = value;
        }
        
    }
}