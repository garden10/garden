﻿namespace Model
{
    public enum PlantType
    {
        Wheat,
        Carrot,
        AppleTree
    }
}