﻿using System;
using UnityEngine;

namespace Model
{
    [Serializable]
    public class ExpManager
    {
        [SerializeField] private float _currentExp;
        [SerializeField] private float _currentCarrot;
        [SerializeField] private float _currentApple;
        [SerializeField] private float _currentWheat;

        public Action HarvestEnd;
        
        public float CurrentExp
        {
            get => _currentExp;
            set => _currentExp = value;
        }

        public float CurrentCarrot
        {
            get => _currentCarrot;
            set => _currentCarrot = value;
        }

        public float CurrentApple
        {
            get => _currentApple;
            set => _currentApple = value;
        }

        public float CurrentWheat
        {
            get => _currentWheat;
            set => _currentWheat = value;
        }

        public void ExpForPlant(Plant plant)
        {
            switch (plant.Type)
            {
                case  PlantType.Carrot:
                    _currentCarrot++;
                    break;
                case  PlantType.AppleTree:
                    _currentApple++;
                    break;
                case  PlantType.Wheat:
                    _currentWheat++;
                    break;
            }
            _currentExp += plant.Exp;
            HarvestEnd?.Invoke();
        }
    }
}