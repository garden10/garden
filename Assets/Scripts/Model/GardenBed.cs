﻿using System;
using UnityEngine;

namespace Model
{
    [Serializable]
    public class GardenBed
    {
         private bool _isFree = true;
         private Plant _currentPlant;
         private Vector3 _position;
         
         public bool IsFree => _isFree;

         public Plant CurrentPlant
         {
             get => _currentPlant;
             set => _currentPlant = value;
         }

         public Vector3 Position
         {
             get => _position;
             set => _position = value;
         }

         public void Planting()
         {
             _isFree = false;
             _currentPlant.Planting();
         }

         public void ReleaseGardenBed()
         {
             if(_currentPlant.IsFaded)
                 _currentPlant.Fade();
             else
             {
                 _isFree = true;
                 _currentPlant.Harvest();
                 _currentPlant = null;
             }
         }
    }
}