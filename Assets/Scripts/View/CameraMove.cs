﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace View
{
    public class CameraMove : MonoBehaviour
    {
        private const float MinDistance = 0.01f;
        
        [SerializeField] private Camera _cam;
        [SerializeField] private float _camSpeed;
        [SerializeField] private float _zoomSpeed;
        [SerializeField] private float _maxZoom;

        private Vector3 _startPosition;
        private float _startZoom;

        private static CameraMove _instance;
        public static CameraMove Instance => _instance;

        private void Start()
        {
            PopupView.IsClicked += GoToStart;
            _startPosition = transform.position;
            _startZoom = _cam.orthographicSize;
            _instance = this;
        }
        

        public void MoveCamera(Vector3 point)
        {
            var destination = _startPosition + point;
            StopAllCoroutines();
            StartCoroutine(GoLayout(destination));
            StartCoroutine(ZoomIn());
        }

        private void GoToStart()
        {
            StopAllCoroutines();
            StartCoroutine(GoLayout(_startPosition));
            StartCoroutine(ZoomOut());
        }

        private void OnDisable() =>
            PopupView.IsClicked -= GoToStart;
        
        
        private IEnumerator GoLayout(Vector3 destination)
        {
            while (Vector3.Distance(transform.position, destination) > MinDistance)
            {
                transform.position = Vector3.MoveTowards(transform.position,
                    destination, _camSpeed * Time.deltaTime);
                yield return null;
            }
        }

        private IEnumerator ZoomIn()
        {
            while (_cam.orthographicSize > _maxZoom)
            {
                _cam.orthographicSize -= _zoomSpeed;
                yield return null;
            }
        }
        private IEnumerator ZoomOut()
        {
            while (_cam.orthographicSize < _startZoom)
            {
                _cam.orthographicSize += _zoomSpeed;
                yield return null;
            }
        }
    }
}