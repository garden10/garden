﻿using System;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class StartGameUI : MonoBehaviour
    {
        [SerializeField] private FieldManagerView _fieldManagerView;

        [SerializeField] private GameObject _startGamePanel;
        
        [SerializeField] private int _maxWidth;
        [SerializeField] private int _maxHeight;
        
        [SerializeField] private TMP_InputField _width;
        [SerializeField] private TMP_InputField _height;

        private void OnEnable()
        {
            _width.onEndEdit.AddListener(delegate { CheckCoefficient(_width, 0, _maxWidth); });
            _height.onEndEdit.AddListener(delegate { CheckCoefficient(_height, 0, _maxHeight); });
        }
        private void CheckCoefficient (TMP_InputField inputField, float numberMin, float numberMax)
        {
            float fieldNumber = 0;
            if(inputField.text.Length != 0)
                fieldNumber = float.Parse(inputField.text, CultureInfo.InvariantCulture.NumberFormat);
            if (fieldNumber <= numberMin || fieldNumber > numberMax)
                inputField.text = Mathf.Clamp(fieldNumber, numberMin, numberMax).ToString(CultureInfo.InvariantCulture); 
        }

        private void OnDisable()
        {
            _width.onEndEdit.RemoveAllListeners();
            _height.onEndEdit.RemoveAllListeners();
        }

        public void SetField()
        {
            _fieldManagerView.FieldManager.Width = int.Parse(_width.text); 
            _fieldManagerView.FieldManager.Height = int.Parse(_height.text); 
            _fieldManagerView.SetField();
            _startGamePanel.SetActive(false);
        }
    }
}