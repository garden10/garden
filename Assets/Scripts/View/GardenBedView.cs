﻿using System;
using System.Collections;
using System.Collections.Generic;
using Model;
using UnityEngine;

namespace View
{
    public class GardenBedView : MonoBehaviour
    {
        private const float MinDistance = 0.01f;
        
        [SerializeField] private GardenBed _gardenBed;
        [SerializeField] private GameObject _popupTemplate;
        [SerializeField] private GameObject[] _plants;
        [SerializeField] private int _layoutSpeed;
        [SerializeField] private PositionCalculator _calculator;

        private List<GameObject> _popups = new List<GameObject>();
        public GardenBed GardenBed => _gardenBed;
        
        public static Action FieldClicked;

        private void Start() =>
            _gardenBed.Position = transform.position;

        private void OnMouseDown()
        {
            FieldClicked?.Invoke();
            if (_gardenBed.IsFree)
            {
                CameraMove.Instance.MoveCamera(transform.position);
                _popups.Clear();
                foreach (var plant in _plants)
                {
                    var newPopup = Instantiate(_popupTemplate, transform);
                    newPopup.transform.position = transform.position;
                    newPopup.GetComponent<PopupView>().CurrentPlant = plant;
                    newPopup.GetComponent<PopupView>().Init();
                    newPopup.GetComponent<MeshCollider>().enabled = false;
                    _popups.Add(newPopup);
                }
                
                _calculator.PositionList.Clear();
                _calculator.FillList(_popups.Count);
                for (int i = 0; i < _popups.Count; i++)
                    StartCoroutine(GoLayout(_layoutSpeed, i));
            }
            else if (_gardenBed.CurrentPlant.CurrentStage == GrowStage.Adult)
                FarmerView.Instance.Farmer.AddQueue(GardenBed);
        }

        public void PrepareForPlanting(GameObject plant)
        {
            var newPlant = Instantiate(plant, transform);
            newPlant.transform.position = transform.position + new Vector3(0,0.01f,0);

            _gardenBed.CurrentPlant = newPlant.GetComponent<PlantView>().Plant;
        }
        
        private IEnumerator GoLayout(float speed, int i)
        {
            while (Vector3.Distance(_popups[i].transform.position, _calculator.PositionList[i]) > MinDistance)
            {
                _popups[i].transform.position = Vector3.MoveTowards(_popups[i].transform.position,
                    _calculator.PositionList[i], speed * Time.deltaTime);
                yield return null;
            }
            _popups[i].GetComponent<MeshCollider>().enabled = true;
        }
    }
}