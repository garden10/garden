﻿using System;
using UnityEngine;

namespace View
{
    public class PopupAnimation : MonoBehaviour
    {
        private static readonly int IsBack = Animator.StringToHash("IsBack");
        private static readonly int IsForward = Animator.StringToHash("IsForward");

        [SerializeField] private Animator _animator;

        private void Back()
        {
            _animator.SetBool(IsBack, true);
            _animator.SetBool(IsForward, false);
        }

        private void Forward()
        {
            _animator.SetBool(IsForward, true);
            _animator.SetBool(IsBack, false);
        }

        private void OnMouseExit() =>
            Back();

        private void OnMouseEnter() =>
            Forward();
    }
}