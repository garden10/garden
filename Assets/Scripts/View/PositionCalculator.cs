﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace View
{
    [Serializable]
    public class PositionCalculator : MonoBehaviour
    {
        [SerializeField] private float _radius;
        [SerializeField] private float _angle;
        [SerializeField] private int _zOffset;

        private Vector3 _popupsPosition;
        private List<Vector3> _positionList = new List<Vector3>();
        public List<Vector3> PositionList => _positionList;
        
        public void FillList(int cardCount)
        {
            var correctionVector = new Vector3(0, 0, -0.1f);
            _popupsPosition = transform.position + correctionVector;

            _positionList.Clear();
            while (_positionList.Count < cardCount)
                _positionList.Add(_popupsPosition);

            CalculatingPositions();
        }


        private void CalculatingPositions()
        {
            var median = _positionList.Count / 2;
            var remainder = _positionList.Count % 2;
            var orderCoefficient = 0.5f;
            if (remainder > 0)
            {
                _positionList[median] = new Vector3(transform.position.x, 0.1f, transform.position.z + _zOffset);

                median += 1;
                orderCoefficient = 1;
            }

            for (int i = median; i < _positionList.Count; i++)
            {
                var cos = Mathf.Cos((i - median + orderCoefficient) * _angle * Mathf.Deg2Rad);
                var sin = Mathf.Sin((i - median + orderCoefficient) * _angle * Mathf.Deg2Rad);
                var newX = transform.position.x + _radius * sin;
                var newZ = _radius * cos;

                var newZCorrection = newZ + transform.position.z + _zOffset - _radius;

                _positionList[i] = new Vector3(newX, 0.1f, newZCorrection);
                _positionList[_positionList.Count - i - 1] = new Vector3(newX - 2 * _radius * sin, 0.1f, newZCorrection);
            }
        }
    }
}