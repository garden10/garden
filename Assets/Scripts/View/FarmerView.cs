﻿using System;
using System.Collections.Generic;
using Model;
using UnityEngine;
using UnityEngine.AI;

namespace View
{
    public class FarmerView : MonoBehaviour
    {
        private static readonly int Lifting = Animator.StringToHash("Lift");
        private static readonly int Go = Animator.StringToHash("Go");
        private static readonly int Hold = Animator.StringToHash("Hold");

        [SerializeField] private Farmer _farmer;

        [SerializeField] private NavMeshAgent _agent;
        [SerializeField] private Animator _animator;
        [SerializeField] private Transform _landMark;
        [SerializeField] private ExpManagerView _expManager;

        public Farmer Farmer => _farmer;

        private static FarmerView _instance;
        public static FarmerView Instance => _instance;


        private void Start()
        {
            _instance = this;
            _farmer.Init(_expManager.ExpManager, transform.position);
            _farmer.IsLifting += LiftFarmer;
            _farmer.IsHolding += HoldFarmer;
            _farmer.SetPoint += SetDestination;
        }

        private void Update() =>
            _farmer.Update(transform.position, _agent.velocity.magnitude);

        private void SetDestination(Vector3 destination)
        {
            _agent.SetDestination(destination);
            transform.LookAt(_agent.destination);
            _animator.SetBool(Lifting, false);
            _animator.SetBool(Hold, false);
            _animator.SetBool(Go, true);
        }

        private void LiftFarmer()
        {
            _animator.SetBool(Lifting, true);
            _animator.SetBool(Go, false);
        }

        private void HoldFarmer()
        {
            _animator.SetBool(Hold, true);
            _animator.SetBool(Go, false);
            transform.LookAt(_landMark);
        }

        /// <summary>
        /// Animation Event
        /// </summary>
        public void PickUp()
        {
            _farmer.FarmerAction();
        }
        
        /// <summary>
        /// Animation Event
        /// </summary>
        public void LiftingEnd()
        {
            _farmer.RemovePlant();
        }

        private void OnDisable()
        {
            _farmer.IsLifting -= LiftFarmer;
            _farmer.IsHolding -= HoldFarmer;
            _farmer.SetPoint -= SetDestination;
        }
    }
}