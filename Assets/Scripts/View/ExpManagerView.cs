﻿using System;
using Model;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class ExpManagerView : MonoBehaviour
    {
        [SerializeField] private ExpManager _expManager;
        
        [SerializeField] private TextMeshProUGUI _expAmount;
        [SerializeField] private TextMeshProUGUI _carrotAmount;
        [SerializeField] private TextMeshProUGUI _appleAmount;
        [SerializeField] private TextMeshProUGUI _wheatAmount;

        public ExpManager ExpManager => _expManager;
        
        private void Awake()
        {
            _expManager.HarvestEnd += UpdateValues;
            UpdateValues();
        }

        private void UpdateValues()
        {
            _expAmount.text = _expManager.CurrentExp.ToString();
            _carrotAmount.text = _expManager.CurrentCarrot.ToString();
            _appleAmount.text = _expManager.CurrentApple.ToString();
            _wheatAmount.text = _expManager.CurrentWheat.ToString();
        }

        private void OnDisable() =>
            _expManager.HarvestEnd -= UpdateValues;
    }
}