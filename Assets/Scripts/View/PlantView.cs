﻿using System;
using Model;
using SO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class PlantView : MonoBehaviour
    {
        [SerializeField] private PlantData _plantData;
        [SerializeField] private Sprite _plantIcon;
        [SerializeField] private GameObject _sprout;
        [SerializeField] private GameObject _juvenile;
        [SerializeField] private GameObject _adult;
        [SerializeField] private GameObject _fade;
        [SerializeField] private TextMeshProUGUI _timeUI;
        [SerializeField] private Plant _plant;

        public Sprite PlantIcon => _plantIcon;
        public Plant Plant => _plant;

        private void Awake()
        {
            _plant.Type = _plantData.Type;
            _plant.GrowTime = _plantData.GrowTime;
            _plant.Exp = _plantData.Exp;
            _plant.IsFaded = _plantData.IsFade;
            _plant.IsHarvest += Harvest;
        }

        private void Update()
        {
            Plant.Update(Time.deltaTime);
            _sprout.SetActive(Plant.CurrentStage == GrowStage.Sprout);
            _juvenile.SetActive(Plant.CurrentStage == GrowStage.Juvenile);
            _adult.SetActive(Plant.CurrentStage == GrowStage.Adult);
            if(_fade != null)
                _fade.SetActive(Plant.CurrentStage == GrowStage.Faded);
            if(_plant.CurrentStage != GrowStage.Seed && _plant.GrowTime - _plant.CurrentTime > 0)
            {
                _timeUI.gameObject.SetActive(true);
                TimeSpan time = TimeSpan.FromSeconds(_plant.GrowTime - _plant.CurrentTime);
                _timeUI.text = time.ToString(@"mm\:ss");
            }
            else
                _timeUI.gameObject.SetActive(false);
        }

        private void Harvest()
        {
            _plant.IsHarvest -= Harvest;
            Destroy(gameObject);
        }
    }
}