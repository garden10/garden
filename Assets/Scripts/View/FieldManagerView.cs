﻿using System;
using Model;
using UnityEngine;

namespace View
{
    public class FieldManagerView : MonoBehaviour
    {
        [SerializeField] private FieldManager _fieldManager;
        
        [SerializeField] private GameObject _gardenBed;
        [SerializeField] private Vector3 _startPoint;
        [SerializeField] private Vector3 _step;

        public FieldManager FieldManager => _fieldManager;

        public void SetField()
        {
            for (int i = 0; i < _fieldManager.Height; i++)
            {
                for (int j = 0; j < _fieldManager.Width; j++)
                {
                    var newGardenBed = Instantiate(_gardenBed);
                    newGardenBed.transform.position = _startPoint + _step * j;
                }
                _startPoint += new Vector3(-_step.x, _step.y, _step.z);
            }
        }

        private void OnMouseDown() =>
            PopupView.IsClicked?.Invoke();
    }
}