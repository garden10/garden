﻿using System;
using Model;
using UnityEngine;
using UnityEngine.UI;

namespace View
{
    public class PopupView : MonoBehaviour
    {
        [SerializeField] private GameObject _currentPlantImage;
        private GameObject _currentPlant;

        public static Action IsClicked; 
        public GameObject CurrentPlant
        {
            get => _currentPlant;
            set => _currentPlant = value;
        }

        public void Init()
        {
            _currentPlantImage.GetComponent<SpriteRenderer>().sprite = _currentPlant.GetComponent<PlantView>().PlantIcon;
            IsClicked += RemovePopup;
            GardenBedView.FieldClicked += RemovePopup;
        }

        private void OnMouseDown()
        {
            transform.parent.GetComponent<GardenBedView>().PrepareForPlanting(_currentPlant);
            
            FarmerView.Instance.Farmer.AddQueue(transform.parent.GetComponent<GardenBedView>().GardenBed);
            IsClicked?.Invoke();
        }

        private void RemovePopup()
        {
            IsClicked -= RemovePopup;
            GardenBedView.FieldClicked -= RemovePopup;
            Destroy(gameObject);
        }
    }
}